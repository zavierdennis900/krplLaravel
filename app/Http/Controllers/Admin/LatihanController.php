<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\latihan;

class LatihanController extends Controller
{
    public function index(): View {
        $latihans = Latihan::where('status_aktif', '=', 'aktif')->get();
        return view('admin/latihan/v_dt_latihan', compact('latihans'));
    }

    public function histori(): View {
        $latihans = Latihan::where('status_aktif', '=', 'hapus')->get();
        return view('admin/latihan/v_dt_histori_latihan', compact('latihans'));
    }

    public function create(): View {
        return view('admin/latihan/v_dt_tambah_latihan');
    }
    public function store(Request $request) {
        //validate form
        $this->validate($request, [
            'kategori'         => 'required|min:8|unique:latihans',
            'status_publish'   => 'required'
        ]);

        $kat = Str::of($request->kategori)->rtrim()->stripTags()->title();
        $slug = Str::slug($request->kategori, '-');

        Latihan::create([
            'kategori'          => $kat,
            'status_publish'    => $request->status_publish,
            'created_by'        => $request->created_by,
            'status_aktif'      => $request->status_aktif,
            'slug_link'         => $slug,
            'created_at'        => NOW()
        ]);

        //redirect to index
        return redirect()->route('latihan.index')->with(['succes' => 'Data Berhasil Ditambah!']);
    }

    public function edit(string $slug_link): View {
        $latihans = Latihan::where('slug_link', '=', '$slug_link')->get();
        return view('admin/latihan/v_dt_edit_latihan', compact('latihans'));
    }

    public function (Request $request, String $slug_link) {
        //validate form
        $this->validate($request, [
            'kategori'         => 'required|min:8|unique:latihans',
            'status_publish'   => 'required'
        ]);

        $kat = Str::of($request->kategori)->rtrim()->stripTags()->title();
        $slug = Str::slug($request->kategori, '-');

        Latihan::update([
            'kategori'          => $kat,
            'status_publish'    => $request->status_publish,
            'updated_by'        => $request->created_by,
            'status_aktif'      => $request->status_aktif,
            'slug_link'         => $slug,
            'updated_at'        => NOW()
        ]);
    }
}
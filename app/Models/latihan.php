<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class latihan extends Model
{
    use HasFactory;

    protected $casts = [
        'crated_at'     => 'datetime',
        'deleted_at'    => 'datetime',
        'updated_at'    => 'datetime'
    ];

    protected $fillable = [
        'kategori', 
        'statu_aktif',
        'status_publish',
        'slug_link',
        'updated_at',
        'deleted_at',
        'updated_by',
        'deleted_by'
    ];
}

@extends('admin/layout/v_layout')

@section('content')
<div class="row">
  <div class="col-10">
        <a href="{{ route('latihan.index') }}" class="btn btn-dark">Kategori</a>
        <a href="{{ route('latihan.histori') }}" class="btn btn-succes">Histori</a>
  </div>
  <div class="col-2">
      <a href="{{ route('latihan.edit', $latihans->slug_link) }}" class="btn btn-primary"><i class="bi bi-building-add"></i>
        EDIT
    </a>

     <!-- validation -->
  <div class="col-12">
    <div class="card border-0 shadow rounded">
      <div class="card-body">
        <form action="{{ route('latihan.update', $latihans->slug_link) }}" method="POST">
          @csrf
          @method('PUT')
          <div class="form-group">
            <label>
              Nama
            </label>
            <input class="form-control @error('kategori') is-invalid @enderror" name="kategori" placeholder="masukkan nama kategori" value="{{ $latihans->kategori }}">
            @error('kategori')
            <div class="alert alert-danger mt-2">
              {{ $message }}
            </div>
            @enderror
          </div>
          <div class="form-group">
            <label>
              Status Publish
            </label>
            <select name="status_publish" class="form-control @error('status_publish') is-invalid @enderror" name="kategori">
              <option value="{{ $latihans->status_publish }}">{{ $latihans->status_publish }}</option>
              <option value="draft">draft</option>
              <option value="publish">publish</option>
            </select>
            @error('status_publish')
            <div class="alert alert-danger mt-2">
              {{ $message }}
            </div>
            @enderror
          </div>
          <input name="updated_by" value="1" type="hidden">
          <input name="status_aktif" value="aktif" type="hidden">
          <button type="reset" class="btn btn-danger">Reset</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>
  <!-- end -->
@endsection
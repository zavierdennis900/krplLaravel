@extends('admin/layout/v_layout')

@section('content')
<div class="row">
    <div class="col-10">
        <a href="{{ route('latihan.index') }}" class="btn btn-success">Kategori</a>
        <span class="text-danger" style="font-size: 4px;">>></span>
        <a href="{{ route('latihan.histori') }}" class="btn btn-dark">Histori</a>
    </div>
    <div class="col-2">
      <a href="{{ route('latihan.create') }}" class="btn btn-primary"><i class="bi bi-building-add"></i>
        TAMBAH
     </a>
    <div class="col-12">
        <table id="example" class="table table-stripped" style="width: 100%;">
            <thead>
                <tr>
                    <th data-priority="1">No</th>
                    <th data-priority="1">Kategori</th>
                    <th data-priority="1">Status Publish</th>
                    <th>Status Aktif</th>
                    <th>Tanggal</th>
                    <th>Penulis</th>
                    <th data-priority="1">Aksi</th>
                </tr>
            </thead>
            <tbody>
            @foreach($latihans as $lat)
                <tr>
                    <td>
                        {{ $lat->id }}
                    </td>
                    <td>
                        {{ $lat->kategori }}
                    </td>
                    <td>
                        {{ $lat->status_publish }}
                    </td>
                    <td>
                        {{ $lat->status_aktif }}
                    </td>
                    <td>
                        @if ($lat->updated != null)
                            {{ $lat->updated_at }}
                            @else
                            {{ $lat->created_at }}
                        @endif
                    </td>
                    <td>Zachdan</td>
                    <td>
                        <a href="#" class="btn btn-primary btn-sm"><i class="bi bi-eye"></i></a>
                        <a href="{{ route('latihan.edit', $lat->slug_link) }}" class="btn btn-warning btn-sm"><i class="bi bi-building-gear"></i></a>
                        <a href="#" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Kategori</th>
                    <th>Status Publish</th>
                    <th>Status Aktif</th>
                    <th>Tanggal</th>
                    <th>Penulis</th>
                    <th>Aksi</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection
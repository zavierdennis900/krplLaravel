<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\LatihanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::controller(LatihanController::class)->group(function () {
    route::get('/latihan', 'index')->name('latihan.index');
    route::get('/histori/latihan', 'histori')->name('latihan.histori');
    route::get('/tambah/latihan', 'create')->name('latihan.create');
    Route::post('/kirim/latihan', 'store')->name('latihan.store');
    Route::post('/ubah/latihan{$slug_link}', 'edit')->name('latihan.edit');
    Route::post('/update/latihan{$slug_link}', 'update')->name('latihan.update');
});
